import 'actions.dart';

int counterReducer(int previousState, dynamic action) {
  if (action == Action.increament) {
    return previousState + 1;
  } else if (action == Action.decreament) {
    return previousState - 1;
  } else {
    return previousState;
  }
}
